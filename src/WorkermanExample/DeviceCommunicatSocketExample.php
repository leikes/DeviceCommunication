<?php 

namespace YouXin\DeviceCommunication\WorkermanExample;

use YouXin\DeviceCommunication\WorkermanUtils\OpenSocketUtil;
use YouXin\DeviceCommunication\WorkermanUtils\FormCheck;

class DeviceCommunicatSocketExample extends OpenSocketUtil{
	public function externalAgreement(){
		$externalAgreement = config('devicecommunicat.external_agreement');

		switch ($externalAgreement) {
			case 'websocket':
				return $externalAgreement;
				break;
			case 'text':
				return $externalAgreement;
				break;
			case 'frame':
				return $externalAgreement;
				break;
			case 'tcp':
				return $externalAgreement;
				break;
			case 'udp':
				return $externalAgreement;
				break;
			case 'unix':
				return $externalAgreement;
				break;
			default:
				echo "使用协议错误\n";
				break;
		}
	}

	public function ip(){
		$ip = config('devicecommunicat.ip');

		$rules = [
			'ip' => 'required|ipv4'
		];

		$formCheck = new FormCheck(['ip' => $ip],$rules);

		if($formCheck){
			return $ip;
		}
	}

	public function insidePort(){
		$insidePort = config('devicecommunicat.insidePort');

		return $insidePort;
	}

	public function externalPort(){
		$externalPort = config('devicecommunicat.externalPort');

		return $externalPort;
	}
}