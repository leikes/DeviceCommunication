<?php 

namespace YouXin\DeviceCommunication\WorkermanUtils;

// use Illuminate\Contracts\Validation\Validator;

class FormCheck{
	public function __construct($data,$rules){
		$validator = \Validator::make($data,$rules);

		if($validator->fails()){
			$this->failedValidation($validator);

			return false;
		}

		return true;
	}

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     *
     * @return void
     */
    protected function failedValidation(Validator $validator)
    {
        echo "配置文件参数有误".$validator->errors()."\n";
    } 
}