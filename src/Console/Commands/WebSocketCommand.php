<?php

namespace YouXin\DeviceCommunication\Console\Commands;

use Illuminate\Console\Command;
use Workerman\Worker;
use Workerman\Lib\Timer;

class WebSocketCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'websocket {action} {-d?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '开启websocket';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        global $argv;
        $arg = $this->argument('action');
        $argv[1] = $argv[2];
        $argv[2] = isset($argv[3]) ? "-{$argv[3]}" : '';

        switch($arg){
            case 'start':
                $this->start();
                break;
            case 'stop':
                $this->stop();
                break;
            case 'restart':
                $this->restart();
                break;
            case 'reload':
                $this->reload();
                break;
            default:
                echo '指令错误，不存在该指令';
                break;
        }
    }

    protected function start(){
        $deviceCommunicatSocketExample = app(\YouXin\DeviceCommunication\WorkermanExample\DeviceCommunicatSocketExample::class);

        $deviceCommunicatSocketExample->start();

        Worker::runAll();
    }

    protected function stop(){
        
        $deviceCommunicatSocketExample = app(\YouXin\DeviceCommunication\WorkermanExample\DeviceCommunicatSocketExample::class);

        $deviceCommunicatSocketExample->stop();

        Worker::runAll();
    }

    protected function restart() {
        
        $deviceCommunicatSocketExample = app(\YouXin\DeviceCommunication\WorkermanExample\DeviceCommunicatSocketExample::class);

        $deviceCommunicatSocketExample->restart();

        Worker::runAll();
    }

    private function reload() {
       $deviceCommunicatSocketExample = app(\YouXin\DeviceCommunication\WorkermanExample\DeviceCommunicatSocketExample::class);

        $deviceCommunicatSocketExample->reload();
        
        Worker::runAll();
    }
}
