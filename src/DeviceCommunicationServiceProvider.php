<?php

namespace YouXin\DeviceCommunication;

use Illuminate\Support\ServiceProvider;

class DeviceCommunicationServiceProvider extends ServiceProvider
{

    // protected $defer = true;
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // dd($this->app->getBindings());

        $this->publishes([
            __DIR__.'/default.config.php'=>config_path('devicecommunicat.php')
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->commands([
            \YouXin\DeviceCommunication\Console\Commands\WebSocketCommand::class
        ]);
    }

    // public function provides(){
    //     return [''];
    // }
}
